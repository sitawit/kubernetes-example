Kubernetes Example
===================================

BASIC
-------------------

kubectl apply -f pod.yaml

kubectl get pods

kubectl logs demo

kubectl delete -f pod.yaml

DEPLOY
-------------------

https://github.com/dockersamples/node-bulletin-board

docker build --tag bulletinboard:1.0 .

kubectl apply -f bb.yaml

kubectl get deployments

kubectl get services

localhost:30001

kubectl delete -f bb.yaml

UI
-------------------

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

kubectl proxy

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

kubectl apply -f dashboard-adminuser.yaml

kubectl apply -f ClusterRoleBinding.yaml

kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | sls admin-user | ForEach-Object { $_ -Split '\s+' } | Select -First 1)